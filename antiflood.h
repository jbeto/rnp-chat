#include <netinet/in.h> // in_addr_t

#ifndef ANTIFLOOD_H
#define ANTIFLOOD_H

static const int ANTIFLOOD_IPSTATUSTYPE_IGNORE = 0;
static const int ANTIFLOOD_IPSTATUSTYPE_ALLOW = 1;

typedef void (*antifloodIPStatusChangedCallback) (in_addr_t ip, int ipStatusType);

typedef struct antifloodListStruct {
  in_addr_t address;
  struct antifloodListStruct* next;
} AntifloodList;

#endif /* ANTIFLOOD_H */

void antifloodInit (int asWhitelist, antifloodIPStatusChangedCallback statusChangedCallback);

void antifloodIgnoreIncomingFrom (in_addr_t addressToIgnore);
void antifloodAllowIncomingFrom (in_addr_t addressToAllow);

int antifloodShouldAddressBeIgnored (in_addr_t addressToCheck);

void antifloodListPrint ();
