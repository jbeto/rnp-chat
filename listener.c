#include "listener.h"

#include <stdio.h> // printf()
#include <sys/socket.h> // socket() bind() listen() shutdown()
#include <string.h> // memset()
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h> // close()

#include "clientListHandler.h"
#include "antiflood.h"

int listenerStart (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], clientHandlerMessageReceivedCallback msgCallback, clientHandlerClientInteractionCallback clientInteractionCallback) {
  ChatProtoClientListEntry clientListEntry;
  ChatProtoLogin login;
  pthread_t listener;
  int socketID;
  struct sockaddr_in socketAddress;
  const int enableReuse = 1;
  ListenerThreadParameter* listenerParameter;

  // Create Socket to listen
  if ((socketID = socket(PF_INET, SOCK_STREAM, proto)) < 0) {
    #ifdef DEBUG_MESSAGES
    printf("listenerStart - socket failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  // set socket address struct
  memset(&socketAddress, 0, sizeof(socketAddress));
  socketAddress.sin_family = AF_INET; // Internet address family
  socketAddress.sin_addr.s_addr = htonl(INADDR_ANY); // Any incoming interface
  socketAddress.sin_port = htons(port); //port to listen

  // Set reuseaddr to Socket
  if (setsockopt(socketID, SOL_SOCKET, SO_REUSEADDR, &enableReuse, sizeof(enableReuse)) != 0) {
    #ifdef DEBUG_MESSAGES
    printf("listenerStart - setsockopt failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  // Bind Address to Socket
  if (bind(socketID, (struct sockaddr *) &socketAddress, sizeof(socketAddress)) < 0) {
    #ifdef DEBUG_MESSAGES
    printf("listenerStart - bind failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  // Mark as listen socket
  if (listen(socketID, MAXPENDING) < 0) {
    #ifdef DEBUG_MESSAGES
    printf("listenerStart - listen failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  // Generate login used for new clients
  chatProtoGenerateLoginStruct(&login, port, name);

  // Add myself to ClientList
  chatProtoGenerateClientListEntryStruct(&clientListEntry, ip, port, name);
  clientListAddClient(socketID, 1, &clientListEntry);
  #ifdef DEBUG_MESSAGES
  clientListPrint();
  #endif /* DEBUG_MESSAGES */

  if ((listenerParameter = malloc(sizeof(ListenerThreadParameter))) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("listenerStart - malloc failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }
  listenerParameter->socketID = socketID;
  listenerParameter->proto = proto;
  memcpy(&listenerParameter->socketAddress, &socketAddress, sizeof(struct sockaddr_in));
  memcpy (&listenerParameter->loginForNewClients, &login, sizeof (ChatProtoLogin));
  listenerParameter->msgCallback = msgCallback;
  listenerParameter->clientInteractionCallback = clientInteractionCallback;

  if (pthread_create(&listener, 0, listenerThread, listenerParameter) != 0) {
    #ifdef DEBUG_MESSAGES
    printf("listenerThread creation failed\n");
    #endif /* DEBUG_MESSAGES */
    free(listenerParameter);
    return -1;
	}

  return socketID;
}

int listenerStop(int socketID) {
  shutdown(socketID, SHUT_RDWR);
  close(socketID);
  return 0;
}

void* listenerThread(void* param) {
  ListenerThreadParameter* listenerParameter = param; // Thread Parameters
  int connectedClientSocketID;
  ChatProtoLogin* loginOfClient;
  struct sockaddr_in connectedClientAddress; // Address of Client that may connect
  unsigned int socketAddressLength = sizeof(connectedClientAddress); // Length of client address data structure

  // Dummy clientLogin erzeugen, der später aktualisiert wird, sobald mehr bekannt ist
  if ((loginOfClient = malloc (sizeof (ChatProtoLogin))) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf ("listenerThread - malloc failed\n");
    #endif /* DEBUG_MESSAGES */
    free (listenerParameter);
    return 0;
  }
  if (chatProtoGenerateLoginStruct(loginOfClient, 0, "nameless Idiot") != 0) {
    free (listenerParameter);
    free (loginOfClient);
    return 0;
  }

  #ifdef DEBUG_MESSAGES
  printf ("listenerThread - listen on Port %d; SocketID: %d\n", ntohs (listenerParameter->socketAddress.sin_port), listenerParameter->socketID);
  #endif /* DEBUG_MESSAGES */

  while (1)
  {
    // wait for possible client
    if ((connectedClientSocketID = accept(listenerParameter->socketID, (struct sockaddr *) &connectedClientAddress, &socketAddressLength)) < 0) {
      #ifdef DEBUG_MESSAGES
      printf ("listenerThread %d - accept failed\n", ntohs (listenerParameter->socketAddress.sin_port));
      #endif /* DEBUG_MESSAGES */
      break;
    }

    if (antifloodShouldAddressBeIgnored (connectedClientAddress.sin_addr.s_addr)) {
      #ifdef DEBUG_MESSAGES
      printf ("listenerThread on Port %d - ignore client %s\n", ntohs (listenerParameter->socketAddress.sin_port), inet_ntoa (connectedClientAddress.sin_addr));
      #endif /* DEBUG_MESSAGES */

      close (connectedClientSocketID);
    } else {
      #ifdef DEBUG_MESSAGES
      printf ("listenerThread on Port %d - handling client %s\n", ntohs (listenerParameter->socketAddress.sin_port), inet_ntoa (connectedClientAddress.sin_addr));
      #endif /* DEBUG_MESSAGES */

      antifloodIgnoreIncomingFrom (connectedClientAddress.sin_addr.s_addr);
      clientHandlerHandleClient (listenerParameter->proto, connectedClientSocketID, connectedClientAddress, &listenerParameter->loginForNewClients, loginOfClient, 0, listenerParameter->msgCallback, listenerParameter->clientInteractionCallback);
    }
  }

  clientListRemoveClient(listenerParameter->socketID);
  free (listenerParameter);
  free (loginOfClient);
  return 0;
}
