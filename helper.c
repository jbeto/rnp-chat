#include "helper.h"

#include <stdio.h> // perror()
#include <stdlib.h> // exit()
#include <errno.h> // errno

void dieWithError(char* errorMessage)
{
  if (errno == 0) errno = -1;
  perror(errorMessage);
  exit(errno);
}
