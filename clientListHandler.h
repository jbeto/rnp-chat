#include "chatproto.h"

#ifndef CLIENTLISTHANDLER_H
#define CLIENTLISTHANDLER_H

typedef struct clientListStruct {
  ChatProtoClientListEntry clientListEntry;
  int onSocket;
  int ownListenSocket;
  struct clientListStruct* next;
} ClientList;

#endif /* CLIENTLISTHANDLER_H */

extern ClientList* clientListFirstEntry;
extern int clientListNumberOfEntries;

int clientListLock(void);
int clientListUnlock(void);

int clientListGetAllSocketsWithClients(int* num, int** sockets);
int clientListExistEntry (ChatProtoClientListEntry* cle);
int clientListAddClient(int socket, int ownListenSocket, ChatProtoClientListEntry* cle);
int clientListHandlerUpdateName (int socket, char name[CHATPROTO_NAMEWIDTH]);
int clientListRemoveClient(int socket);

void clientListPrint(void);
