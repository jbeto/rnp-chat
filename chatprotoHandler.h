#include <stdint.h> // uint8_t uint16_t
#include <netinet/in.h> // in_addr_t

#include "chatproto.h"

#ifndef CHATPROTOHANDLER_H
#define CHATPROTOHANDLER_H

typedef void (*chatprotoHandlerMessageReceivedCallback) (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int length, char message[length]);
typedef void (*chatprotoHandlerClientInteractionCallback) (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int clientInteractionType);
typedef void (*chatprotoHandlerIPStatusChangedCallback) (in_addr_t ip, int ipStatusType);

#endif /* CHATPROTOHANDLER_H */

int chatprotoHandlerInit (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], chatprotoHandlerMessageReceivedCallback msgCallback, chatprotoHandlerClientInteractionCallback clientInteractionCallback, chatprotoHandlerIPStatusChangedCallback ipStatusChangedCallback);
int chatprotoHandlerConnectTo (int proto, in_addr_t ip, uint16_t port);
int chatprotoHandlerSendMessage(int length, char message[length]);
int chatprotoHandlerGetClientList (int* num, ChatProtoClientListEntry** entry);
void chatprotoHandlerIgnoreIncomingFrom (in_addr_t addressToIgnore);
void chatprotoHandlerAllowIncomingFrom (in_addr_t addressToAllow);
int chatprotoHandlerExit(void);
