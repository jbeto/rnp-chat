CC      = gcc
CFLAGS  = -Wall -g3 -Wextra
#CFLAGS += -DDEBUG_MESSAGES
#CFLAGS += -DTRACE_MESSAGES
LDFLAGS = -lpthread -lsctp

SRC	= main.c helper.c chatproto.c chatprotoHandler.c clientListHandler.c listener.c clientHandler.c antiflood.c
PROG	= chat

NICESRC = niceclient.c chatproto.c chatprotoHandler.c clientListHandler.c listener.c clientHandler.c antiflood.c
NICEPROG = nicechat

OBJS    = $(SRC:%.c=%.o)
NICEOBJS = $(NICESRC:%.c=%.o)
DEPENDS = $(SRC:%.c=%.d) $(NICESRC:%.c=%.d)

all: $(PROG) $(NICEPROG)

debug: CFLAGS += -DDEBUG_MESSAGES
debug: rebuild

trace: CFLAGS += -DTRACE_MESSAGES
trace: debug

$(PROG) : $(OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS) $(LDFLAGS)

$(NICEPROG) : $(NICEOBJS)
	$(CC) $(CFLAGS) -o $(NICEPROG) $(NICEOBJS) $(LDFLAGS)

%.o: %.c
	$(CC) -MD $(CFLAGS) -c $<

-include $(DEPENDS)

.PHONY: clean
clean:
	rm -rf $(OBJS) $(NICEOBJS) $(DEPENDS)

.PHONY: distclean
distclean: clean
	rm -rf $(PROG) $(NICEPROG)

.PHONY: rebuild
rebuild: distclean all
