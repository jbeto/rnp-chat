#include "clientListHandler.h"

#include <stdlib.h>
#include <stdio.h> //printf()
#include <pthread.h>
#include <string.h> //memcpy()

ClientList* clientListFirstEntry = NULL;
ClientList* lastEntry = NULL;
int clientListNumberOfEntries = 0;

pthread_mutex_t clientListHandlerMtx = PTHREAD_MUTEX_INITIALIZER;

inline int clientListLock(void) {
  return pthread_mutex_lock(&clientListHandlerMtx);
}

inline int clientListUnlock(void) {
  return pthread_mutex_unlock(&clientListHandlerMtx);
}


ClientList* clientListHandlerGetEntryOfSocket (int socket) {
  ClientList* currentEntry = clientListFirstEntry;

  while (currentEntry != NULL) {
    if (currentEntry->onSocket == socket) {
      return currentEntry;
    }

    currentEntry = currentEntry->next;
  }

  return NULL;
}


int clientListGetAllSocketsWithClients(int* num, int** sockets) {
  int index = 0;
  ClientList* currentEntry;

  if (*sockets != NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientListGetAllSocketsWithClients - sockets not NULL");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  clientListLock();
  currentEntry = clientListFirstEntry;
  *num = clientListNumberOfEntries;
  if ((*sockets = malloc(sizeof(int) * clientListNumberOfEntries)) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientListGetAllSocketsWithClients - malloc failed");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  for (index = 0; index < clientListNumberOfEntries; index++) {
    (*sockets)[index] = currentEntry->onSocket;
    currentEntry = currentEntry->next;
  }

  clientListUnlock();
  return 0;
}

int clientListHandlerExistEntryInternal (ChatProtoClientListEntry* cle) {
  ClientList* currentEntry = clientListFirstEntry;
  int socketID;

  #ifdef TRACE_MESSAGES
  printf ("clientListExistEntry - search this one:\n");
  chatProtoPrintClientListEntry (cle);
  #endif /* TRACE_MESSAGES */

  while (currentEntry != NULL) {
    #ifdef TRACE_MESSAGES
    printf ("clientListExistEntry - compare with this one:\n");
    chatProtoPrintClientListEntry (&currentEntry->clientListEntry);
    #endif /* TRACE_MESSAGES */

    // Check only for ip / port; ignore name!
    if (memcmp (&cle->ip, &currentEntry->clientListEntry.ip, sizeof (in_addr_t)) == 0) {
      #ifdef TRACE_MESSAGES
      printf ("clientListExistEntry - matching ip\n");
      #endif /* TRACE_MESSAGES */

      if (cle->port == currentEntry->clientListEntry.port) {
        socketID = currentEntry->onSocket;

        #ifdef TRACE_MESSAGES
        printf ("clientListExistEntry - found matching one!\n");
        #endif /* TRACE_MESSAGES */

        return socketID;
      }
    }

    #ifdef TRACE_MESSAGES
    printf ("clientListExistEntry - continue with next on %p\n", currentEntry->next);
    #endif /* TRACE_MESSAGES */
    currentEntry = currentEntry->next;
  }

  #ifdef TRACE_MESSAGES
  printf ("clientListExistEntry - nothing found :(\n");
  #endif /* TRACE_MESSAGES */

  return -1;
}

int clientListExistEntry (ChatProtoClientListEntry* cle) {
  int result = 0;

  clientListLock ();
  result = clientListHandlerExistEntryInternal (cle);
  clientListUnlock();

  return result;
}

int clientListAddClient(int socket, int ownListenSocket, ChatProtoClientListEntry* cle) {
  ClientList* newEntry;

  clientListLock();

  if (clientListHandlerExistEntryInternal (cle) >= 0) {
    #ifdef DEBUG_MESSAGES
    printf("clientListAddClient - already in list\n");
    #endif /* DEBUG_MESSAGES */
    clientListUnlock();
    return -1;
  }

  if ((newEntry = malloc(sizeof(ClientList))) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientListAddClient - malloc failed\n");
    #endif /* DEBUG_MESSAGES */
    clientListUnlock();
    return -1;
  }
  memcpy(&newEntry->clientListEntry, cle, sizeof(ChatProtoClientListEntry));
  newEntry->onSocket = socket;
  newEntry->ownListenSocket = ownListenSocket;
  newEntry->next = NULL;

  if (lastEntry != NULL)
    lastEntry->next = newEntry;
  lastEntry = newEntry;
  if (clientListFirstEntry == NULL) {
    clientListFirstEntry = newEntry;
  }

  clientListNumberOfEntries++;

  clientListUnlock();
  return 0;
}

int clientListHandlerUpdateName (int socket, char name[CHATPROTO_NAMEWIDTH]) {
  ClientList* entry = NULL;

  clientListLock ();
  if ((entry = clientListHandlerGetEntryOfSocket (socket)) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf ("clientListHandlerUpdateName - entry of socket %d not in List\n", socket);
    #endif /* DEBUG_MESSAGES */
    clientListUnlock ();
    return -1;
  }
  strncpy (entry->clientListEntry.name, name, CHATPROTO_NAMEWIDTH);

  clientListUnlock ();
  return 0;
}

int clientListRemoveClient(int socket) {
  ClientList* entryBefore = NULL;
  ClientList* currentEntry = NULL;

  clientListLock();

  currentEntry = clientListFirstEntry;

  while (currentEntry != NULL) {
    if (currentEntry->onSocket == socket) {
      if (entryBefore == NULL) {
        clientListFirstEntry = currentEntry->next;
      } else {
        entryBefore->next = currentEntry->next;
      }

      if (currentEntry == lastEntry) {
        lastEntry = entryBefore;
      }

      free(currentEntry);
      clientListNumberOfEntries--;
      clientListUnlock();
      return 0;
    }

    entryBefore = currentEntry;
    currentEntry = currentEntry->next;
  }
  clientListUnlock();
  return -1;
}

void clientListPrint(void) {
  ClientList* currentEntry = NULL;
  int i = 0;
  clientListLock();
  currentEntry = clientListFirstEntry;

  printf("Start of List %p\nElements: %d\n", clientListFirstEntry, clientListNumberOfEntries);
  while (currentEntry != NULL) {
    printf("Index %d on %p\n", i++, currentEntry);
    chatProtoPrintClientListEntry(&currentEntry->clientListEntry);
    printf("Socket: %d\n", currentEntry->onSocket);
    printf("Next: %p\n", currentEntry->next);
    currentEntry = currentEntry->next;
  }
  printf("End of List\n\n");
  clientListUnlock();
}
