#include <stdio.h>
#include <stdlib.h> // atoi() exit()
#include <netdb.h> // getaddrinfo() struct addrinfo
#include <arpa/inet.h> // inet_pton()
#include <unistd.h> // usleep()
#include <string.h> // strlen()
#include <time.h> // localtime() asctime()
#include <signal.h>
#include <execinfo.h> // print_backtrace()
#include <ctype.h> // tolower()

#include "chatproto.h"
#include "chatprotoHandler.h"
#include "clientHandler.h"
#include "antiflood.h"
#include "helper.h"

void loop (void);

void signal_shutdown (int signum __attribute__ ((unused)));
void signal_termination (int signum);

void messageReceived (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int length, char message[length]);
void clientInteraction (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int clientInteractionType);
void ipStatusChanged (in_addr_t ip, int ipStatusType);

static const int INPUT_BUFFER_LENGTH = 80;
static const char* INPUT_READIN_MESSAGE = " %79[^\n]%*c";
static const char* INPUT_READIN_COMMAND = "%79s";
static const char* INPUT_READIN_IP = "%15s";

#define PROTO_COLOR_TCP "\e[0;33m"
#define PROTO_COLOR_SCTP "\e[0;32m"
#define PROTO_COLOR_OTHER "\e[0;35m"

int main(int argc, char *argv[]) {
	int proto;
	struct addrinfo hints;
	struct addrinfo *listen;
	struct sockaddr_in* listenAddress;
	char* name;
	struct addrinfo *connectto;
	struct sockaddr_in* connecttoAddress;

	if (argc != 5 && argc != 7)
	{
		fprintf (stderr, "Usage:  %s <tcp|sctp> <own IP> <own Port> <own Name> [<connect to server ip> <connect to server port>]\n", argv[0]);
		exit(1);
	}

	signal (SIGABRT, signal_termination); // Process abort signal
	signal (SIGBUS, signal_termination); // Access to an undefined portion of a memory object
	signal (SIGFPE, signal_termination); // Erroneous arithmetic operation
	signal (SIGSEGV, signal_termination); // Invalid memory reference
	signal (SIGQUIT, signal_termination); // Terminal quit signal

	signal (SIGTERM, signal_shutdown); // Termination signal
	signal (SIGINT, signal_shutdown); // Terminal interrupt signal

	if (strncmp (argv[1], "tcp", 4) == 0) {
		proto = IPPROTO_TCP;
	}	else if (strncmp (argv[1], "sctp", 5) == 0) {
		proto = IPPROTO_SCTP;
	} else {
		dieWithError("not tcp or sctp");
	}

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	// listener ip, port
	if (getaddrinfo(argv[2], argv[3], &hints, &listen) != 0) {
		dieWithError("get listener AddressInfo failed");
	}
	listenAddress = (struct sockaddr_in*) listen->ai_addr;

	name = argv[4];
	if (argc == 7) {
		// connectto ip, port
		if (getaddrinfo(argv[5], argv[6], &hints, &connectto) != 0) {
			dieWithError("get connectto AddressInfo failed");
		}
		connecttoAddress = (struct sockaddr_in*) connectto->ai_addr;
	}

	if (chatprotoHandlerInit (proto, listenAddress->sin_addr.s_addr, ntohs (listenAddress->sin_port), name, messageReceived, clientInteraction, ipStatusChanged) != 0) {
		dieWithError("chatprotoHandlerInit failed");
	}

	printf ("I am \e[1;36m%s\e[m on "
			"\e[0;34m%s\e[m"
			"%s:%u\e[m\n",
			name, inet_ntoa (listenAddress->sin_addr), PROTO_COLOR_OTHER, ntohs (listenAddress->sin_port));

	if (argc == 7) {
		if (chatprotoHandlerConnectTo(proto, connecttoAddress->sin_addr.s_addr, ntohs (connecttoAddress->sin_port)) != 0) {
			dieWithError("chatprotoHandlerConnectTo failed");
		}
	}

	loop ();

	freeaddrinfo (listen);
	freeaddrinfo (connectto);

	return 0;
}

void saybyebye (void) {
	printf ("Going down...\n");
	chatprotoHandlerSendMessage (5, "Bye!");
	chatprotoHandlerExit ();
	usleep (500000);
	exit (0);
}

void clientlist (void) {
	char nameBuffer[CHATPROTO_NAMEWIDTH];
	int numberOfEntries;
	ChatProtoClientListEntry* entry;
	int i;

	if (chatprotoHandlerGetClientList (&numberOfEntries, &entry) != 0) {
		dieWithError ("chatprotoHandlerGetClientList failed");
	}

	printf ("Current Users: (%d)\n", numberOfEntries);
	for (i = 0; i < numberOfEntries; i++) {
		strncpy (nameBuffer, entry[i].name, CHATPROTO_NAMEWIDTH);
		nameBuffer[CHATPROTO_NAMEWIDTH] = '\0';

		printf ("\t"
		"\e[1;36m%16s\e[m on "
		"\e[0;34m%15s\e[m"
		"\e[0;35m:%5u\e[m\n",
		nameBuffer, inet_ntoa ((struct in_addr){entry[i].ip}), ntohs (entry[i].port));
	}
}

void longmsg (void) {
	char stringBuffer[CHATPROTO_MAX_MESSAGE_CHARS];
	int i;

	for (i = 0; i < CHATPROTO_MAX_MESSAGE_CHARS - 1; i++) {
		stringBuffer[i] = (char)(0x30 + i % 10);
	}

	stringBuffer[CHATPROTO_MAX_MESSAGE_CHARS - 1] = '\0';

	chatprotoHandlerSendMessage (strlen (stringBuffer) + 1, stringBuffer);
}

void charbomb (char letter) {
	char stringBuffer[CHATPROTO_MAX_MESSAGE_CHARS];
	int i;

	for (i = 0; i < CHATPROTO_MAX_MESSAGE_CHARS - 1; i++) {
		stringBuffer[i] = letter;
	}

	stringBuffer[CHATPROTO_MAX_MESSAGE_CHARS - 1] = '\0';

	chatprotoHandlerSendMessage (strlen (stringBuffer) + 1, stringBuffer);
}

void sendInvisibleMessage(int length, char message[length]) {
	char stringBuffer[CHATPROTO_MAX_MESSAGE_CHARS];
	int i;

	memset (stringBuffer, 0, sizeof (stringBuffer));

	for (i = 0; i < length - 1; i++) {
		stringBuffer[i] = message[i];
	}
	for (i = 0; i < length - 1; i++) {
		stringBuffer[length - 1 + i] = '\b';
	}

	chatprotoHandlerSendMessage (strlen (stringBuffer) + 1, stringBuffer);
}

void help (int beUseless) {
	if (beUseless) {
		printf ("\tnope.\n");
	} else {
		printf ("Help:\n"
			"\tallow <ip>\t\tallow incomming connections from ip - counterpart: ignore\n"
			"\tbackspacebomb\t\tsend extreme high amount of the Backspace Chars. May it delete the opposite client terminals!\n"
			"\tbeep\t\t\tsend \\a to trigger the console beep sound\n"
			"\tclientlist\t\tshow the list of currently connected clients\n"
			"\tcolortroll\t\tpaint the client terminals in \e[30;41manother\e[m color\n"
			"\tcolortroll-black\tpaint the client terminals in \e[30;40mblack\e[m color\n"
			"\tcolortroll-dark\t\tpaint the client terminals in \e[37;40mdark\e[m color theme\n"
			"\tcolortroll-light\tpaint the client terminals in \e[30;47mlight\e[m color theme\n"
			"\tcolortroll-white\tpaint the client terminals in \e[37;47mwhite\e[m color\n"
			"\tdeletebomb\t\tsend extreme high amount of the Delete Chars. May it delete the opposite client terminals!\n"
			"\texit\t\t\tclose the client\n"
			"\thelp\t\t\tshow this help\n"
			"\tignore <ip>\t\tignore incomming connections from ip - counterpart: allow\n"
			"\tinfo\t\t\tsame as clientlist\n"
			"\tinvis <text>\t\tsend a message with the exact number of backspaces after it\n"
			"\tlongmsg\t\t\tsend the max possible message length full of numbers\n"
			"\tmsg <text>\t\tto send a message text to all clients\n"
			"\tsemiclear\t\tsend extreme high amout of \\v (vertical tab)\n"
		);
	}
}

void loop (void) {
	char stringBuffer[INPUT_BUFFER_LENGTH];
	in_addr_t address;
	int i;
	int lastInputIsUnknown = 1; // ist der erste Command help -> nope :P

	while (1) {
		scanf (INPUT_READIN_COMMAND, stringBuffer);
		for (i = 0; stringBuffer[i]; i++){
			stringBuffer[i] = tolower (stringBuffer[i]);
		}

		if (strncmp (stringBuffer, "msg", INPUT_BUFFER_LENGTH - 1) == 0) {
			scanf (INPUT_READIN_MESSAGE, stringBuffer);
			chatprotoHandlerSendMessage (strlen (stringBuffer) + 1, stringBuffer);
		} else if (strncmp (stringBuffer, "allow", INPUT_BUFFER_LENGTH - 1) == 0) {
			scanf (INPUT_READIN_IP, stringBuffer);
			inet_pton (AF_INET, stringBuffer, &address);
			printf("\tAllow Address: %s\n", inet_ntoa((struct in_addr){address}));
			chatprotoHandlerAllowIncomingFrom (address);
		} else if (strncmp (stringBuffer, "backspacebomb", INPUT_BUFFER_LENGTH - 1) == 0) {
			charbomb ('\b');
		} else if (strncmp (stringBuffer, "beep", INPUT_BUFFER_LENGTH - 1) == 0) {
			chatprotoHandlerSendMessage (2, "\a");
		} else if (strncmp (stringBuffer, "clientlist", INPUT_BUFFER_LENGTH - 1) == 0) {
			clientlist ();
		} else if (strncmp (stringBuffer, "colortroll", INPUT_BUFFER_LENGTH - 1) == 0) {
			chatprotoHandlerSendMessage (9, "\e[30;41m");
		} else if (strncmp (stringBuffer, "colortroll-black", INPUT_BUFFER_LENGTH - 1) == 0) {
			chatprotoHandlerSendMessage (9, "\e[30;40m");
		} else if (strncmp (stringBuffer, "colortroll-dark", INPUT_BUFFER_LENGTH - 1) == 0) {
			chatprotoHandlerSendMessage (9, "\e[37;40m");
		} else if (strncmp (stringBuffer, "colortroll-light", INPUT_BUFFER_LENGTH - 1) == 0) {
			chatprotoHandlerSendMessage (9, "\e[30;47m");
		} else if (strncmp (stringBuffer, "colortroll-white", INPUT_BUFFER_LENGTH - 1) == 0) {
			chatprotoHandlerSendMessage (9, "\e[37;47m");
		} else if (strncmp (stringBuffer, "deletebomb", INPUT_BUFFER_LENGTH - 1) == 0) {
			charbomb (0x7F);
		} else if (strncmp (stringBuffer, "ignore", INPUT_BUFFER_LENGTH - 1) == 0) {
			scanf (INPUT_READIN_IP, stringBuffer);
			inet_pton (AF_INET, stringBuffer, &address);
			printf("\tIgnore Address: %s\n", inet_ntoa((struct in_addr){address}));
			chatprotoHandlerIgnoreIncomingFrom (address);
		} else if (strncmp (stringBuffer, "info", INPUT_BUFFER_LENGTH - 1) == 0) {
			clientlist ();
		} else if (strncmp (stringBuffer, "invis", INPUT_BUFFER_LENGTH - 1) == 0) {
			scanf (INPUT_READIN_MESSAGE, stringBuffer);
			sendInvisibleMessage (strlen (stringBuffer) + 1, stringBuffer);
		} else if (strncmp (stringBuffer, "exit", INPUT_BUFFER_LENGTH - 1) == 0) {
			saybyebye ();
		} else if (strncmp (stringBuffer, "help", INPUT_BUFFER_LENGTH - 1) == 0) {
			help (lastInputIsUnknown == 1);
		} else if (strncmp (stringBuffer, "longmsg", INPUT_BUFFER_LENGTH - 1) == 0) {
			longmsg ();
		} else if (strncmp (stringBuffer, "semiclear", INPUT_BUFFER_LENGTH - 1) == 0) {
			charbomb ('\v');
		} else {
			printf ("\tdont know that command '%s'... im sorry... not!\n"
				"\tmaybe 'help' will help you :)\n",
				stringBuffer);
			lastInputIsUnknown = 0;
		}

		lastInputIsUnknown++;
	}
}

void print_backtrace (void) {
  void *buffer[255];
  const int calls = backtrace (buffer, sizeof (buffer) / sizeof (void*));
  backtrace_symbols_fd (buffer, calls, 1);
}

void signal_shutdown (int signum __attribute__ ((unused))) {
	//INTERRUPT Shutdown
	signal (signum, signal_termination);

	printf ("\n"); //gibt ^C auf der Konsole aus -> Zeilenumbruch um weitere Nachrichten wieder leichter Lesbar darzustellen
	#ifdef DEBUG_MESSAGES
	printf ("a shutdown signal was received\n");
	#endif /* DEBUG_MESSAGES */

	saybyebye ();
}

void signal_termination (int signum) {
	printf ("Termination by Signal %d\n", signum);
	print_backtrace ();
	dieWithError ("FATAL ERROR");
}

void messageReceived (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int length, char message[length]) {
	char timeBuffer[1024];
	char nameBuffer[CHATPROTO_NAMEWIDTH+1];
	char messageBuffer[CHATPROTO_MAX_MESSAGE_CHARS];
	int messageBufferPosition = 0;
	char* protoColor = NULL;
	time_t rawtime;
	int i;
	int readableCharCounter = 0;
	time(&rawtime);

	if (proto == IPPROTO_TCP) {
		protoColor = PROTO_COLOR_TCP;
	} else if (proto == IPPROTO_SCTP) {
		protoColor = PROTO_COLOR_SCTP;
	} else {
		protoColor = PROTO_COLOR_OTHER;
	}

	strncpy (nameBuffer, name, CHATPROTO_NAMEWIDTH);
	nameBuffer[CHATPROTO_NAMEWIDTH] = '\0';
	strftime (timeBuffer, sizeof (timeBuffer), "%T", localtime(&rawtime));
	memset (messageBuffer, 0, CHATPROTO_MAX_MESSAGE_CHARS);

	for (i = 0; i < length - 1; i++) {
		if (message[i] >= 0x20 &&
			message[i] <= 0x7E) {
			messageBuffer[messageBufferPosition++] = message[i];
			readableCharCounter++;
		}
	}

	if (readableCharCounter == 0) {
		printf("\e[0;36m%s\e[m "
			"\e[0;34m%15s\e[m"
			"%s:%5u\e[m "
			"\e[1;36m%16s\e[m "
			"\e[0;31msent nothing readable\e[m\n",
			timeBuffer, inet_ntoa ((struct in_addr){ip}), protoColor, port, nameBuffer);
	} else {
		printf("\e[0;36m%s\e[m "
			"\e[0;34m%15s\e[m"
			"%s:%5u\e[m "
			"\e[1;36m%16s\e[m "
			"\e[0;33m%s\e[m\n",
			timeBuffer, inet_ntoa ((struct in_addr){ip}), protoColor, port, nameBuffer, messageBuffer);
	}
}

#define MESSAGE_TEXT_LOGIN "\e[1;32mappears with a bang\e[m"
#define MESSAGE_TEXT_DETECTED "\e[1;32mappears in the fog\e[m"
#define MESSAGE_TEXT_NAMECHANGED "\e[1;32mhas a better name now\e[m"
#define MESSAGE_TEXT_LOGOUT "\e[0;31mfled\e[m"
#define MESSAGE_TEXT_UNKNOWN "\e[1;31mexploded\e[m";

void clientInteraction (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int clientInteractionType) {
	char* protoColor = NULL;
	char* message = NULL;
	char nameBuffer[CHATPROTO_NAMEWIDTH+1];
	char timeBuffer[1024];
	time_t rawtime;
	time(&rawtime);

	if (proto == IPPROTO_TCP) {
		protoColor = PROTO_COLOR_TCP;
	} else if (proto == IPPROTO_SCTP) {
		protoColor = PROTO_COLOR_SCTP;
	} else {
		protoColor = PROTO_COLOR_OTHER;
	}

	if (clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGIN) {
		message = MESSAGE_TEXT_LOGIN;
	} else if (clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_DETECTED) {
		message = MESSAGE_TEXT_DETECTED;
	} else if (clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_NAMECHANGED) {
		message = MESSAGE_TEXT_NAMECHANGED;
  } else if (clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGOUT) {
		message = MESSAGE_TEXT_LOGOUT;
	} else {
		message = MESSAGE_TEXT_UNKNOWN;
	}

	strncpy (nameBuffer, name, CHATPROTO_NAMEWIDTH);
	nameBuffer[CHATPROTO_NAMEWIDTH] = '\0';
	strftime (timeBuffer, sizeof (timeBuffer), "%T", localtime(&rawtime));

	printf("\e[0;36m%s\e[m "
		"\e[0;34m%15s\e[m"
		"%s:%5u\e[m "
		"\e[1;36m%16s\e[m "
		"%s\n",
		timeBuffer, inet_ntoa ((struct in_addr){ip}), protoColor, port, nameBuffer, message);

	if (clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGIN ||
			clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_DETECTED) {
		snprintf (timeBuffer, sizeof (timeBuffer), "Hello %16s!", nameBuffer);
	} else if (clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGOUT) {
		snprintf (timeBuffer, sizeof (timeBuffer), "See you %16s!", nameBuffer);
	} else if (clientInteractionType == CLIENTHANDLER_CLIENTINTERACTIONTYPE_UNKNOWN) {
		snprintf (timeBuffer, sizeof (timeBuffer), "What happened to %16s?", nameBuffer);
	}
}

#define MESSAGE_TEXT_IPALLOW "\e[1;33mIP allowed now\e[m"
#define MESSAGE_TEXT_IPIGNORE "\e[0;31mIP ignored now\e[m"

void ipStatusChanged (in_addr_t ip, int ipStatusType) {
	char* message = NULL;
	char timeBuffer[1024];
	time_t rawtime;
	time(&rawtime);

	if (ipStatusType == ANTIFLOOD_IPSTATUSTYPE_ALLOW) {
		message = MESSAGE_TEXT_IPALLOW;
	} else if (ipStatusType == ANTIFLOOD_IPSTATUSTYPE_IGNORE) {
		message = MESSAGE_TEXT_IPIGNORE;
	} else {
		message = MESSAGE_TEXT_UNKNOWN;
	}

	strftime (timeBuffer, sizeof (timeBuffer), "%T", localtime(&rawtime));

	printf("\e[0;36m%s\e[m "
		"\e[0;34m%15s\e[m    "
		"%s\n",
		timeBuffer, inet_ntoa ((struct in_addr){ip}), message);
}
