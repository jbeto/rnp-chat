#include "chatprotoHandler.h"

#include <stdio.h> //printf()
#include <string.h>
#include <stdlib.h>

#include "listener.h"
#include "clientHandler.h"
#include "clientListHandler.h"
#include "antiflood.h"

int listenerSocketID;
in_addr_t myIp;
uint16_t myPort;
char myName[CHATPROTO_NAMEWIDTH];

int chatprotoHandlerRunning = 0;


chatprotoHandlerMessageReceivedCallback msgFunction;
chatprotoHandlerClientInteractionCallback clientInteractionFunction;
chatprotoHandlerIPStatusChangedCallback clientprotoHandlerIPStatusChangedFunction;

void chatprotoHandlerMessageReceived (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int socketID, int length, char message[length]);
void chatprotoHandlerClientInteraction (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int socketID, int clientInteractionType);
void chatprotoHandlerIPStatusChanged (in_addr_t ip, int ipStatusType);

int chatprotoHandlerInit (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], chatprotoHandlerMessageReceivedCallback msgCallback, chatprotoHandlerClientInteractionCallback clientInteractionCallback, chatprotoHandlerIPStatusChangedCallback ipStatusChangedCallback) {
  // Recognize the callbacks
  msgFunction = msgCallback;
  clientInteractionFunction = clientInteractionCallback;
  clientprotoHandlerIPStatusChangedFunction = ipStatusChangedCallback;

  myIp = ip;
  myPort = port;
  strncpy(myName, name, CHATPROTO_NAMEWIDTH);

  antifloodInit (0, chatprotoHandlerIPStatusChanged);

  // Start Listener
  if ((listenerSocketID = listenerStart (proto, ip, port, name, chatprotoHandlerMessageReceived, chatprotoHandlerClientInteraction)) < 0) {
		printf("chatprotoHandlerInit - listenerStart failed\n");
    return -1;
  }

  chatprotoHandlerRunning = 1;

  return 0;
}

int chatprotoHandlerConnectTo (int proto, in_addr_t ip, uint16_t port) {
  ChatProtoLogin login;

  if (chatProtoGenerateLoginStruct(&login, myPort, myName) != 0) {
    return -1;
  }

  if (clientHandlerConnectToClient (proto, ip, port, "initial Server", &login, chatprotoHandlerMessageReceived, chatprotoHandlerClientInteraction) < 0) {
    return -1;
  }

  return 0;
}

int chatprotoHandlerSendMessage(int length, char message[length]) {
  ClientList* currentEntry = NULL;
  int sendtocounter = 0;

  clientListLock();
  currentEntry = clientListFirstEntry;

  while (currentEntry != NULL) {
    if (!currentEntry->ownListenSocket) {
      if (clientHandlerSendMessage(currentEntry->onSocket, length, message) != 0) {
        clientListUnlock();
        return -1;
      }
      sendtocounter++;
    }

    currentEntry = currentEntry->next;
  }

  if (sendtocounter == 0) {
    printf ("Noone there... you're alone!\n");
    clientListUnlock();
    return -1;
  }

  clientListUnlock();
  (*msgFunction) (0, myIp, myPort, myName, length, message);

  return 0;
}

int chatprotoHandlerGetClientList (int* num, ChatProtoClientListEntry** entry __attribute__ ((unused))) {
  ClientList* currentEntry = NULL;
  ChatProtoClientListEntry* cle = NULL;
  int i = 0;

  clientListLock();
  currentEntry = clientListFirstEntry;

  if ((cle = malloc ((clientListNumberOfEntries - 1) * sizeof (ClientList))) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf ("chatprotoHandlerGetClientList - malloc failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  while (currentEntry != NULL) {
    if (!currentEntry->ownListenSocket) {
      memcpy (&cle[i], &currentEntry->clientListEntry, sizeof (ChatProtoClientListEntry));
      #ifdef TRACE_MESSAGES
      chatProtoPrintClientListEntry(&cle[i]);
      #endif /* TRACE_MESSAGES */
      i++;
    }

    currentEntry = currentEntry->next;
  }

  #ifdef TRACE_MESSAGES
  printf("End of List\n\n");
  #endif /* TRACE_MESSAGES */

  clientListUnlock();

  *num = i;
  *entry = cle;

  return 0;
}

void chatprotoHandlerIgnoreIncomingFrom (in_addr_t addressToIgnore) {
  antifloodIgnoreIncomingFrom (addressToIgnore);
}

void chatprotoHandlerAllowIncomingFrom (in_addr_t addressToAllow) {
  antifloodAllowIncomingFrom (addressToAllow);
}

int chatprotoHandlerExit(void) {
  chatprotoHandlerRunning = 0;

  listenerStop(listenerSocketID);

  ClientList* currentEntry = NULL;

  clientListLock();
  currentEntry = clientListFirstEntry;

  while (currentEntry != NULL) {
    if (!currentEntry->ownListenSocket) {
      clientHandlerDisconnect(currentEntry->onSocket);
    }

    currentEntry = currentEntry->next;
  }

  clientListUnlock();

  return 0;
}

void chatprotoHandlerMessageReceived (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int socketID, int length, char message[length]) {
  socketID = socketID;
  if (chatprotoHandlerRunning)
    (*msgFunction)(proto, ip, port, name, length, message);

  free(message);
}

void chatprotoHandlerClientInteraction (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int socketID, int clientInteractionType) {
  socketID = socketID;
  if (chatprotoHandlerRunning)
    (*clientInteractionFunction)(proto, ip, port, name, clientInteractionType);
}

void chatprotoHandlerIPStatusChanged (in_addr_t ip, int ipStatusType) {
  (*clientprotoHandlerIPStatusChangedFunction) (ip, ipStatusType);
}
