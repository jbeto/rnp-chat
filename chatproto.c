#include "chatproto.h"

#include <string.h> //memset() strncpy()
#include <stdio.h> //printf()
#include <arpa/inet.h> //inet_ntoa()

int chatProtoGenerateCommonHeaderStruct(ChatProtoCommonHeader* generated, uint8_t type, uint16_t length) {
  memset(generated, 0, sizeof(ChatProtoCommonHeader));
  generated->version = CHATPROTO_VERSION;
  generated->type = type;
  generated->length = htons (length);

  return 0;
}

int chatProtoGenerateLoginStruct(ChatProtoLogin* generated, uint16_t port, char name[CHATPROTO_NAMEWIDTH]) {
  memset(generated, 0, sizeof(ChatProtoLogin));
  generated->port = htons (port);
  strncpy(generated->name, name, CHATPROTO_NAMEWIDTH);

  return 0;
}

int chatProtoGenerateClientListEntryStruct(ChatProtoClientListEntry* generated, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH]) {
  memset(generated, 0, sizeof(ChatProtoClientListEntry));
  generated->ip = ip;
  generated->port = htons (port);
  strncpy(generated->name, name, CHATPROTO_NAMEWIDTH);

  return 0;
}


void chatProtoPrintCommonHeader(ChatProtoCommonHeader* header) {
  printf("\e[0;31m%p\e[m  ChatProtoCommonHeader\n"
            "\tVersion: \e[0;33m%u\e[m\n"
            "\tType:    \e[0;33m%u\e[m\n"
            "\tLength:  \e[0;33m%u\e[m\n",
    header, header->version, header->type, ntohs (header->length));
}

void chatProtoPrintLogin(ChatProtoLogin* login) {
  printf("\e[0;31m%p\e[m  ChatProtoLogin\n"
            "\tPort:    \e[0;33m%u\e[m\n"
            "\tName:    \e[0;33m%s\e[m\n",
    login, ntohs (login->port), login->name);
}

void chatProtoPrintClientListEntry(ChatProtoClientListEntry* clientListEntry) {
  printf("\e[0;31m%p\e[m  ChatProtoClientListEntry\n"
            "\tIP:      \e[0;33m%s\e[m\n"
            "\tPort:    \e[0;33m%u\e[m\n"
            "\tName:    \e[0;33m%s\e[m\n",
    clientListEntry, inet_ntoa((struct in_addr){clientListEntry->ip}), ntohs (clientListEntry->port), clientListEntry->name);
}
