#include <arpa/inet.h> // sockaddr_in in_addr_t

#include "chatproto.h"

#ifndef CLIENTHANDLER_H
#define CLIENTHANDLER_H

static const int CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGIN = 0;
static const int CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGOUT = 1;
static const int CLIENTHANDLER_CLIENTINTERACTIONTYPE_DETECTED = 2;
static const int CLIENTHANDLER_CLIENTINTERACTIONTYPE_NAMECHANGED = 3;
static const int CLIENTHANDLER_CLIENTINTERACTIONTYPE_UNKNOWN = -1;

typedef void (*clientHandlerMessageReceivedCallback) (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int socketID, int length, char message[length]);
typedef void (*clientHandlerClientInteractionCallback) (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int socketID, int clientInteractionType);

typedef struct clientHandlerThreadParameterStruct {
  int socketID;
  int proto;
  struct sockaddr_in address;
  ChatProtoLogin loginForNewClients;
  ChatProtoLogin loginOfClient;
  int initiatedConnection; // true / false
  clientHandlerMessageReceivedCallback msgCallback;
  clientHandlerClientInteractionCallback clientInteractionCallback;
} ClientHandlerThreadParameter;

#endif /* CLIENTHANDLER_H */

int clientHandlerConnectToClient (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], ChatProtoLogin* loginForNewClient, clientHandlerMessageReceivedCallback msgCallback, clientHandlerClientInteractionCallback clientInteractionCallback);
int clientHandlerHandleClient (int proto, int socketID, struct sockaddr_in address, ChatProtoLogin* loginForNewClients, ChatProtoLogin* loginOfClient, int initiatedConnection, clientHandlerMessageReceivedCallback msgCallback, clientHandlerClientInteractionCallback clientInteractionCallback);
int clientHandlerDisconnect(int socketID);

int clientHandlerSendMessage(int socketID, int length, char message[length]);
