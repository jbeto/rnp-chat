#include "clientHandler.h"

#include <stdio.h> // printf()
#include <string.h> // memset() strncpy()
#include <sys/socket.h> // recv() send()
#include <unistd.h> // close()
#include <pthread.h> // pthread_t pthread_create()
#include <signal.h> // Signalhandler
#include <stdlib.h> // exit()
//#include <malloc.h>

#include "chatproto.h"
#include "clientListHandler.h"
#include "antiflood.h"

// Hilfreiche Webseiten für Socket Programmierung
// http://beej.us/guide/bgnet/output/html/multipage/index.html
// http://pubs.opengroup.org/onlinepubs/7908799/xns/syssocket.h.html

int clientHandlerSendLogin(int socketID, ChatProtoLogin* login);
int clientHandlerSendLogout(int socketID);
int clientHandlerSendClientList(int socketID);

void* clientHandlerThread(void* param);

int handleTypeLogin(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login);
int handleTypeLogout(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login);
int handleTypeMessage(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login);
int handleTypeClientList(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login);

int clientHandlerConnectToClient (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], ChatProtoLogin* loginForNewClient, clientHandlerMessageReceivedCallback msgCallback, clientHandlerClientInteractionCallback clientInteractionCallback) {
  int socketID;
  struct sockaddr_in address;
  ChatProtoClientListEntry clientListEntry;
  ChatProtoLogin* clientLogin;

  #ifdef DEBUG_MESSAGES
  printf("clientHandlerConnectToClient - connectTo Address: %s, Port: %d, Name: %16s\n", inet_ntoa ((struct in_addr){ip}), port, name);
  #endif /* DEBUG_MESSAGES */

  // Create Socket
  if ((socketID = socket(PF_INET, SOCK_STREAM, proto)) < 0) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerConnectToClient - socket failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  memset (&clientListEntry, 0, sizeof (ChatProtoClientListEntry));
  memcpy (&clientListEntry.ip, &ip, sizeof (in_addr_t));
  clientListEntry.port = htons (port);
  strncpy (clientListEntry.name, name, CHATPROTO_NAMEWIDTH);

  if (clientListAddClient (socketID, 0, &clientListEntry) != 0) {
    #ifdef DEBUG_MESSAGES
    printf ("clientHandlerConnectToClient - add to ClientList failed\n");
    #endif /* DEBUG_MESSAGES */
    close (socketID);
    return -1;
  }

  clientInteractionCallback (proto, ip, port, clientListEntry.name, socketID, CLIENTHANDLER_CLIENTINTERACTIONTYPE_DETECTED);

  // set socket address struct
  memset(&address, 0, sizeof(address));
  address.sin_family      = AF_INET;
  address.sin_addr.s_addr = ip;
  address.sin_port        = htons(port);

  // connect to Server
  if (connect(socketID, (struct sockaddr *) &address, sizeof(address)) < 0) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerConnectToClient - connect failed\n");
    #endif /* DEBUG_MESSAGES */
    clientListRemoveClient (socketID);
    close (socketID);
    return -1;
  }

  if ((clientLogin = malloc (sizeof (ChatProtoLogin))) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerConnectToClient - malloc failed\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }
  if (chatProtoGenerateLoginStruct(clientLogin, port, name) != 0) {
    return -1;
  }

  if (clientHandlerHandleClient (proto, socketID, address, loginForNewClient, clientLogin, 1, msgCallback, clientInteractionCallback) != socketID) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerConnectToClient - handle client failed\n");
    #endif /* DEBUG_MESSAGES */
    clientListRemoveClient (socketID);
    close (socketID);
    free (clientLogin);
    return -1;
  }

  free (clientLogin);

  if (clientHandlerSendLogin (socketID, loginForNewClient) != 0) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerConnectToClient - sendLogin failed\n");
    #endif /* DEBUG_MESSAGES */
    clientListRemoveClient (socketID);
    close (socketID);
    return -1;
  }

  return socketID;
}

int clientHandlerHandleClient (int proto, int socketID, struct sockaddr_in address, ChatProtoLogin* loginForNewClients, ChatProtoLogin* loginOfClient, int initiatedConnection, clientHandlerMessageReceivedCallback msgCallback, clientHandlerClientInteractionCallback clientInteractionCallback) {
  pthread_t clientHandler;
  ClientHandlerThreadParameter* clientHandlerParameter;

  if ((clientHandlerParameter = malloc(sizeof(ClientHandlerThreadParameter))) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerHandleClient - malloc failed\n");
    #endif /* DEBUG_MESSAGES */
    clientListRemoveClient (socketID);
    close (socketID);
    return -1;
  }
  clientHandlerParameter->socketID = socketID;
  clientHandlerParameter->proto = proto;
  clientHandlerParameter->address = address;
  memcpy (&clientHandlerParameter->loginForNewClients, loginForNewClients, sizeof (ChatProtoLogin));
  memcpy (&clientHandlerParameter->loginOfClient, loginOfClient, sizeof (ChatProtoLogin));
  clientHandlerParameter->initiatedConnection = initiatedConnection;
  clientHandlerParameter->msgCallback = msgCallback;
  clientHandlerParameter->clientInteractionCallback = clientInteractionCallback;

  if (pthread_create(&clientHandler, 0, clientHandlerThread, clientHandlerParameter) != 0) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerHandleClient - clientHandlerThread creation failed\n");
    #endif /* DEBUG_MESSAGES */
    clientListRemoveClient (socketID);
    close (socketID);
    return -1;
  }

  return socketID;
}

int clientHandlerDisconnect(int socketID) {
  clientHandlerSendLogout(socketID);

  if (shutdown(socketID, SHUT_RDWR) != 0) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerDisconnect %d - shutdown failed\n", socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  } else {
    #ifdef TRACE_MESSAGES
    printf("clientHandlerDisconnect %d - shutdown successfull\n", socketID);
    #endif /* TRACE_MESSAGES */
  }

  close(socketID);
  return 0;
}

int clientHandlerSendLogin(int socketID, ChatProtoLogin* login) {
  void* package;
  ssize_t sendSize;

  #ifdef DEBUG_MESSAGES
  printf("clientHandlerSendLogin %d - send Login\n", socketID);
  #endif /* DEBUG_MESSAGES */

  if ((package = malloc(CHATPROTO_LENGTH_LOGIN)) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendLogin %d - malloc failed\n", socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  // Generate the commonHeader
  chatProtoGenerateCommonHeaderStruct((ChatProtoCommonHeader*)package, CHATPROTO_TYPE_LOGIN, CHATPROTO_LENGTH_LOGIN);

  // Add the Login Struct
  memcpy((ChatProtoLogin*)(package + sizeof(ChatProtoCommonHeader)), login, sizeof(ChatProtoLogin));

  // Send the package
  if ((sendSize = send(socketID, package, CHATPROTO_LENGTH_LOGIN, 0)) != CHATPROTO_LENGTH_LOGIN) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendLogin %d - sent a different number of bytes: %zu, expected: %u\n", socketID, sendSize, CHATPROTO_LENGTH_LOGIN);
    #endif /* DEBUG_MESSAGES */
    free(package);
    return -1;
  } else {
    #ifdef TRACE_MESSAGES
    printf("clientHandlerSendLogin %d - sent successfull\n", socketID);
    #endif /* TRACE_MESSAGES */
  }

  free(package);
  return 0;
}

int clientHandlerSendLogout(int socketID) {
  void* package;
  ssize_t sendSize;

  #ifdef DEBUG_MESSAGES
  printf("clientHandlerSendLogout %d - send Logout\n", socketID);
  #endif /* DEBUG_MESSAGES */

  if ((package = malloc(CHATPROTO_LENGTH_LOGOUT)) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendLogout %d - malloc failed\n", socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }
  // Generate the commonHeader
  chatProtoGenerateCommonHeaderStruct((ChatProtoCommonHeader*)package, CHATPROTO_TYPE_LOGOUT, CHATPROTO_LENGTH_LOGOUT);

  // Send the package
  if ((sendSize = send(socketID, package, CHATPROTO_LENGTH_LOGOUT, 0)) != CHATPROTO_LENGTH_LOGOUT) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendLogout %d - send logout sent a different number of bytes: %zu, expected: %u\n", socketID, sendSize, CHATPROTO_LENGTH_LOGOUT);
    #endif /* DEBUG_MESSAGES */
    free(package);
    return -1;
  } else {
    #ifdef TRACE_MESSAGES
    printf("clientHandlerSendLogout %d - sent logout successfull\n", socketID);
    #endif /* TRACE_MESSAGES */
  }

  free(package);
  return 0;
}

int clientHandlerSendMessage(int socketID, int length, char message[length]) {
  void* package = NULL;
  #ifdef DEBUG_MESSAGES
  printf("clientHandlerSendMessage %d - send: %s\n", socketID, message);
  #endif /* DEBUG_MESSAGES */

  if (length > CHATPROTO_MAX_MESSAGE_CHARS) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendMessage - message length is %d, maximum is %d. To long!\n", length, CHATPROTO_MAX_MESSAGE_CHARS);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  if (message[length-1] != '\0') {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendMessage - message does not end with 0\n");
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  if ((package = malloc(sizeof(ChatProtoCommonHeader) + length)) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendMessage %d - malloc failed\n", socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  // Generate the commonHeader
  chatProtoGenerateCommonHeaderStruct((ChatProtoCommonHeader*)package, CHATPROTO_TYPE_MESSAGE, sizeof(ChatProtoCommonHeader) + length);

  // Add the Message
  strncpy((char*)(package + sizeof(ChatProtoCommonHeader)), message, length);


  // Send the package
  if (send(socketID, package, sizeof(ChatProtoCommonHeader) + length, 0) != (ssize_t)(sizeof(ChatProtoCommonHeader) + length)) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendMessage %d - sent a different number of bytes than expected\n", socketID);
    #endif /* DEBUG_MESSAGES */
    free(package);
    return -1;
  } else {
    #ifdef TRACE_MESSAGES
    printf("clientHandlerSendMessage %d - sent successfull: %s\n", socketID, message);
    #endif /* TRACE_MESSAGES */
  }

  free(package);
  return 0;
}

int clientHandlerSendClientList(int socketID) {
  void* package;
  ssize_t packageLength;
  ChatProtoClientListEntry* currentChatProtoClientListEntry;
  ClientList* currentEntry;
  int index = 0;
  ssize_t sendSize;

  #ifdef DEBUG_MESSAGES
  printf("clientHandlerSendClientList %d - send ClientList\n", socketID);
  clientListPrint();
  #endif /* DEBUG_MESSAGES */
  clientListLock();

  if (clientListNumberOfEntries > CHATPROTO_MAX_CLIENTLIST_ENTRYS) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendClientList %d - too much clientListEntries: have %d, maximum %d\n", socketID, clientListNumberOfEntries, CHATPROTO_MAX_CLIENTLIST_ENTRYS);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  packageLength = sizeof(ChatProtoCommonHeader) + clientListNumberOfEntries * sizeof(ChatProtoClientListEntry);

  if ((package = malloc(packageLength)) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendClientList %d - malloc failed\n", socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  // Generate the commonHeader
  chatProtoGenerateCommonHeaderStruct((ChatProtoCommonHeader*)package, CHATPROTO_TYPE_CLIENTLIST, packageLength);

  currentEntry = clientListFirstEntry;
  while (currentEntry != NULL) {
    currentChatProtoClientListEntry = (ChatProtoClientListEntry*)(package + sizeof(ChatProtoCommonHeader) + index * sizeof(ChatProtoClientListEntry));
    memcpy(currentChatProtoClientListEntry, &currentEntry->clientListEntry, sizeof(ChatProtoClientListEntry));

    index++;
    currentEntry = currentEntry->next;
  }

  clientListUnlock();

  // Send the package
  if ((sendSize = send(socketID, package, packageLength, 0)) != packageLength) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerSendClientList %d - sent a different number of bytes: %zu, expected: %zu\n", socketID, sendSize, packageLength);
    #endif /* DEBUG_MESSAGES */
    free(package);
    return -1;
  } else {
    #ifdef TRACE_MESSAGES
    printf("clientHandlerSendClientList %d - sent successfull\n", socketID);
    #endif /* TRACE_MESSAGES */
  }

  free(package);
  return 0;
}

void* clientHandlerThread(void* param) {
  ClientHandlerThreadParameter* clientHandlerParameter = param; // Thread Parameters
  ChatProtoCommonHeader commonHeader;
  ssize_t length = 0;
  // Erstmal den schon bekannten Port übernehmen
  int loggedIn = clientHandlerParameter->initiatedConnection;

  #ifdef DEBUG_MESSAGES
  printf("clientHandlerThread %d - recv\n", clientHandlerParameter->socketID);
  #endif /* DEBUG_MESSAGES */
  while (1) {
    // receive commonHeader
    if ((length = recv(clientHandlerParameter->socketID, &commonHeader, sizeof(ChatProtoCommonHeader), 0)) < (ssize_t)sizeof(ChatProtoCommonHeader)) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - recv common header failed - length is %zu, expected %zu\n", clientHandlerParameter->socketID, length, sizeof (ChatProtoCommonHeader));
      #endif /* DEBUG_MESSAGES */
      break;
    }

    if (commonHeader.version != 1) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - version is %d, expected %d\n", clientHandlerParameter->socketID, commonHeader.version, CHATPROTO_VERSION);
      #endif /* DEBUG_MESSAGES */
      break;
    }

    //TODO: was passiert bei falschen Längen im Payload? bekommt die Common Header danach nicht mehr mit...


    if (loggedIn == 1 && commonHeader.type == CHATPROTO_TYPE_LOGIN) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - another login?\n", clientHandlerParameter->socketID);
      #endif /* DEBUG_MESSAGES */
      break;
    }

    if (!loggedIn && commonHeader.type != CHATPROTO_TYPE_LOGIN) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - should login first\n", clientHandlerParameter->socketID);
      #endif /* DEBUG_MESSAGES */
      break;
    }

    if (commonHeader.type == CHATPROTO_TYPE_LOGIN) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - login\n", clientHandlerParameter->socketID);
      #endif /* DEBUG_MESSAGES */
      if (handleTypeLogin(clientHandlerParameter, &commonHeader, &clientHandlerParameter->loginOfClient) != 0)
        break;
      loggedIn = 1;
    } else if (commonHeader.type == CHATPROTO_TYPE_LOGOUT) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - logout\n", clientHandlerParameter->socketID);
      #endif /* DEBUG_MESSAGES */
      if (handleTypeLogout(clientHandlerParameter, &commonHeader, &clientHandlerParameter->loginOfClient) != 0)
        break;
      loggedIn = 0;
      break;
    } else if (commonHeader.type == CHATPROTO_TYPE_MESSAGE) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - message\n", clientHandlerParameter->socketID);
      #endif /* DEBUG_MESSAGES */
      if (handleTypeMessage(clientHandlerParameter, &commonHeader, &clientHandlerParameter->loginOfClient) != 0)
        break;
    } else if (commonHeader.type == CHATPROTO_TYPE_CLIENTLIST) {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - client list\n", clientHandlerParameter->socketID);
      #endif /* DEBUG_MESSAGES */
      if (handleTypeClientList(clientHandlerParameter, &commonHeader, &clientHandlerParameter->loginOfClient) != 0)
        break;
    } else {
      #ifdef DEBUG_MESSAGES
      printf("clientHandlerThread %d - unknown type: %d\n", clientHandlerParameter->socketID, commonHeader.type);
      #endif /* DEBUG_MESSAGES */
      break;
    }
  }

  clientListRemoveClient(clientHandlerParameter->socketID);
  close(clientHandlerParameter->socketID); // close Socket
  #ifdef DEBUG_MESSAGES
  printf("clientHandlerThread %d - close\n", clientHandlerParameter->socketID);
  chatProtoPrintLogin(&clientHandlerParameter->loginOfClient);
  clientListPrint();
  #endif /* DEBUG_MESSAGES */
  if (loggedIn) {// something went wrong...
    clientHandlerParameter->clientInteractionCallback (clientHandlerParameter->proto, clientHandlerParameter->address.sin_addr.s_addr, ntohs (clientHandlerParameter->loginOfClient.port), clientHandlerParameter->loginOfClient.name, clientHandlerParameter->socketID, CLIENTHANDLER_CLIENTINTERACTIONTYPE_UNKNOWN);
  }
  free(clientHandlerParameter);

  return 0;
}

int handleTypeLogin(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login) {
  ChatProtoClientListEntry clientListEntry;

  if (ntohs (commonHeader->length) != CHATPROTO_LENGTH_LOGIN) {
    #ifdef DEBUG_MESSAGES
    printf ("handleTypeLogin %d - login length is %d, expected: %d\n", clientHandlerParameter->socketID, ntohs (commonHeader->length), CHATPROTO_LENGTH_LOGIN);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  if (recv(clientHandlerParameter->socketID, login, sizeof(ChatProtoLogin), 0) < (ssize_t)sizeof(ChatProtoLogin)) {
    #ifdef DEBUG_MESSAGES
    printf ("handleTypeLogin %d - recv login failed\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }
  chatProtoGenerateClientListEntryStruct (&clientListEntry, clientHandlerParameter->address.sin_addr.s_addr, ntohs (login->port), login->name);
  if (clientListAddClient (clientHandlerParameter->socketID, 0, &clientListEntry) != 0) {
    #ifdef DEBUG_MESSAGES
    printf ("handleTypeLogin %d - add to ClientList failed\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  antifloodAllowIncomingFrom (clientHandlerParameter->address.sin_addr.s_addr);

  clientHandlerSendClientList(clientHandlerParameter->socketID);

  #ifdef DEBUG_MESSAGES
  chatProtoPrintLogin(login);
  clientListPrint();
  #endif /* DEBUG_MESSAGES */

  clientHandlerParameter->clientInteractionCallback (clientHandlerParameter->proto, clientHandlerParameter->address.sin_addr.s_addr, ntohs (login->port), login->name, clientHandlerParameter->socketID, CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGIN);

  return 0;
}

int handleTypeLogout(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login) {
  if (ntohs (commonHeader->length) != CHATPROTO_LENGTH_LOGOUT) {
    #ifdef DEBUG_MESSAGES
    printf("clientHandlerThread %d - logoff length is %d, expected: %d\n", clientHandlerParameter->socketID, ntohs (commonHeader->length), CHATPROTO_LENGTH_LOGOUT);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }
  clientHandlerParameter->clientInteractionCallback (clientHandlerParameter->proto, clientHandlerParameter->address.sin_addr.s_addr, ntohs (login->port), login->name, clientHandlerParameter->socketID, CLIENTHANDLER_CLIENTINTERACTIONTYPE_LOGOUT);

  return 0;
}

int handleTypeMessage(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login) {
  char* message = NULL;
  ssize_t expectedLength = ntohs (commonHeader->length) - sizeof(ChatProtoCommonHeader);

  if (expectedLength <= 0) {
    #ifdef DEBUG_MESSAGES
    printf("handleTypeMessage %d - extreme short message (<= 0 chars)\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  if ((message = malloc(expectedLength)) == NULL) {
    printf("handleTypeMessage %d - malloc failed\n", clientHandlerParameter->socketID);
    return -1;
  }

  if (recv(clientHandlerParameter->socketID, message, expectedLength, 0) < expectedLength) {
    #ifdef DEBUG_MESSAGES
    printf("handleTypeMessage %d - recv message failed\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    free(message);
    return -1;
  }

  if (message[expectedLength-1] != '\0') {
    #ifdef DEBUG_MESSAGES
    printf("handleTypeMessage %d - message does not end with 0\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    free(message);
    return -1;
  }

  #ifdef DEBUG_MESSAGES
  printf("handleTypeMessage %d - message: %s\n", clientHandlerParameter->socketID, message);
  #endif /* DEBUG_MESSAGES */
  clientHandlerParameter->msgCallback (clientHandlerParameter->proto, clientHandlerParameter->address.sin_addr.s_addr, ntohs (login->port), login->name, clientHandlerParameter->socketID, expectedLength, message);

  return 0;
}

int handleTypeClientList(ClientHandlerThreadParameter* clientHandlerParameter, ChatProtoCommonHeader* commonHeader, ChatProtoLogin* login) {
  ChatProtoClientListEntry* clientListEntry = NULL;
  int expectedEntries = (ntohs (commonHeader->length) - sizeof(ChatProtoCommonHeader)) / sizeof(ChatProtoClientListEntry);
  int isPackageLengthIdeal = !((ntohs (commonHeader->length) - sizeof(ChatProtoCommonHeader)) % sizeof(ChatProtoClientListEntry));
  int index;

  if (!isPackageLengthIdeal) {
    #ifdef DEBUG_MESSAGES
    printf("handleTypeClientList %d - client list length wrong\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  #ifdef DEBUG_MESSAGES
  printf("handleTypeClientList %d - client list expectedEntries: %d\n", clientHandlerParameter->socketID, expectedEntries);
  #endif /* DEBUG_MESSAGES */

  if (expectedEntries <= 0) {
    #ifdef DEBUG_MESSAGES
    printf("handleTypeClientList %d - less than 1 entry?\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    return -1;
  }

  if ((clientListEntry = malloc(sizeof(ChatProtoClientListEntry) * expectedEntries)) == NULL) {
    printf("handleTypeClientList %d - malloc failed\n", clientHandlerParameter->socketID);
    return -1;
  }

  if (recv(clientHandlerParameter->socketID, clientListEntry, sizeof(ChatProtoClientListEntry) * expectedEntries, 0) < (int)sizeof(ChatProtoClientListEntry) * expectedEntries) {
    #ifdef DEBUG_MESSAGES
    printf("handleTypeClientList %d - recv clientListEntry failed\n", clientHandlerParameter->socketID);
    #endif /* DEBUG_MESSAGES */
    free(clientListEntry);
    return -1;
  }

  for (index = 0; index < expectedEntries; index++) {
    #ifdef DEBUG_MESSAGES
    printf ("handleTypeClientList %d - client list entry to handle:\n", clientHandlerParameter->socketID);
    chatProtoPrintClientListEntry(&clientListEntry[index]);
    #endif /* DEBUG_MESSAGES */

    if (memcmp(&clientHandlerParameter->address.sin_addr.s_addr, &clientListEntry[index].ip, sizeof(in_addr_t)) == 0 &&
        clientHandlerParameter->address.sin_port == clientListEntry[index].port) {
      // Mein gegenüber

      #ifdef TRACE_MESSAGES
      printf ("Found my opposite. His current name: %16s\n"
              "        how he named himself:        %16s\n", login->name, clientListEntry[index].name);
      #endif /* TRACE_MESSAGES */

      // login den richtigen Namen geben
      if (strncmp (login->name, clientListEntry[index].name, CHATPROTO_NAMEWIDTH) != 0) {
        strncpy(login->name, clientListEntry[index].name, CHATPROTO_NAMEWIDTH);
        clientListHandlerUpdateName (clientHandlerParameter->socketID, clientListEntry[index].name);
        #ifdef DEBUG_MESSAGES
        printf("handleTypeClientList %d - now known as %s\n", clientHandlerParameter->socketID, login->name);
        #endif /* DEBUG_MESSAGES */

        clientHandlerParameter->clientInteractionCallback (clientHandlerParameter->proto, clientHandlerParameter->address.sin_addr.s_addr, ntohs (login->port), login->name, clientHandlerParameter->socketID, CLIENTHANDLER_CLIENTINTERACTIONTYPE_NAMECHANGED);
      }
    } else if (clientListExistEntry (&clientListEntry[index]) == -1) {
      // Es besteht keine Verbindung zu diesem Eintrag

      #ifdef TRACE_MESSAGES
      printf ("handleTypeClientList %d - found not connected Client in ClientList\n", clientHandlerParameter->socketID);
      #endif /* TRACE_MESSAGES */

      // Mal gucken ob er brauchbar antwortet...
      clientHandlerConnectToClient (clientHandlerParameter->proto, clientListEntry[index].ip, ntohs (clientListEntry[index].port), clientListEntry[index].name, &clientHandlerParameter->loginForNewClients, clientHandlerParameter->msgCallback, clientHandlerParameter->clientInteractionCallback);
    }
  }

  free(clientListEntry);
  return 0;
}
