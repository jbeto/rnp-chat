#include <stdio.h>
#include <stdlib.h> // atoi() exit()
#include <arpa/inet.h> // inet_pton()
#include <unistd.h> // usleep()
#include <string.h> // strlen()

#include "chatproto.h"
#include "chatprotoHandler.h"
#include "clientHandler.h"

void messageReceived(in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int length, char message[length]) {
  printf ("%s %u %s %d %s", inet_ntoa ((struct in_addr){ip}), port, name, length, message);
}
void clientInteraction(in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], int clientInteractionType) {
  printf ("%s %u %s %d", inet_ntoa ((struct in_addr){ip}), port, name, clientInteractionType);
}


// old and lazy: /dev/urandom
int urandom (int proto, in_addr_t connecttoIP, uint16_t connecttoPort) {
  int socketID;
  struct sockaddr_in address;
  FILE* file = NULL;
  char c;
  int count = 0;

  // Create Socket
  if ((socketID = socket (PF_INET, SOCK_STREAM, proto)) < 0) {
    printf ("urandom - socket failed\n");
    return -1;
  }

  // set socket address struct
  memset (&address, 0, sizeof (address));
  address.sin_family      = AF_INET;
  address.sin_addr.s_addr = connecttoIP;
  address.sin_port        = htons(connecttoPort);

  // connect to Server
  if (connect (socketID, (struct sockaddr *) &address, sizeof (address)) < 0) {
    printf ("urandom - connect failed\n");
    close (socketID);
    return -1;
  }

  file = (FILE*) fopen ("/dev/urandom", "rb");
  if ((file) == 0) {
    printf ("urandom - fopen failed\n");
    close (socketID);
    return -1;
  }

  while ((c = fgetc (file)) != EOF && count < 1000000) {
    if (send (socketID, &c, sizeof (char), 0) != 1) {
      printf ("urandom - send failed\n");
      break;
    }
    count++;
  }

  fclose (file);
  close (socketID);
  return 0;
}

int urandomOften (int proto, in_addr_t connecttoIP, uint16_t connecttoPort) {
  int i;
  int times = 0;

  for (i = 0; i < 5000; i++) {
    if (urandom (proto, connecttoIP, connecttoPort) != 0) {
      printf ("urandomOften - client down after urandom connections: %d\n", times);
      return -1;
    }
    times++;
  }

  printf ("urandomOften - client handled all %d urandom connections\n", times);

  return 0;
}

// Verbindung aufbauen aber kein login senden
int justConnect (int proto, in_addr_t connecttoIP, uint16_t connecttoPort) {
  int socketID;
  struct sockaddr_in address;

  // Create Socket
  if ((socketID = socket (PF_INET, SOCK_STREAM, proto)) < 0) {
    printf ("justConnect - socket failed\n");
    return -1;
  }

  // set socket address struct
  memset (&address, 0, sizeof (address));
  address.sin_family      = AF_INET;
  address.sin_addr.s_addr = connecttoIP;
  address.sin_port        = htons(connecttoPort);

  // connect to Server
  if (connect (socketID, (struct sockaddr *) &address, sizeof (address)) < 0) {
    printf ("justConnect - connect failed\n");
    close (socketID);
    return -1;
  }

  return 0;
}

int justConnectOften (int proto, in_addr_t connecttoIP, uint16_t connecttoPort) {
  int i;
  int times = 0;

  for (i = 0; i < 5000; i++) {
    if (justConnect (proto, connecttoIP, connecttoPort) != 0) {
      printf ("justConnectOften - client down after connects: %d\n", times);
      return -1;
    }
    times++;
  }

  printf ("justConnectOften - client handled all %d connections\n", times);

  return 0;
}

// Dauerhaft Logins senden
int constantLogins (int proto, in_addr_t connecttoIP, uint16_t connecttoPort) {
  int socketID;
  struct sockaddr_in address;
  void* package = NULL;
  int count = 0;

  if ((package = malloc (CHATPROTO_LENGTH_LOGIN)) == NULL) {
    printf ("constantLogins - malloc failed\n");
    return -1;
  }

  chatProtoGenerateCommonHeaderStruct (package, CHATPROTO_TYPE_LOGIN, CHATPROTO_LENGTH_LOGIN);
  chatProtoGenerateLoginStruct (package + sizeof (ChatProtoCommonHeader), 0, "HAHA! IDIOT!");

  // Create Socket
  if ((socketID = socket (PF_INET, SOCK_STREAM, proto)) < 0) {
    printf ("constantLogins - socket failed\n");
    free (package);
    return -1;
  }

  // set socket address struct
  memset (&address, 0, sizeof (address));
  address.sin_family      = AF_INET;
  address.sin_addr.s_addr = connecttoIP;
  address.sin_port        = htons(connecttoPort);

  // connect to Server
  if (connect (socketID, (struct sockaddr *) &address, sizeof (address)) < 0) {
    perror ("constantLogins - connect failed\n");
    close (socketID);
    free (package);
    return -1;
  }

  for (count = 0; count < 1000000; count++) {
    if (send (socketID, package, CHATPROTO_TYPE_LOGIN, 0) != CHATPROTO_TYPE_LOGIN) {
      printf ("constantLogins - send failed\n");
      break;
    }
  }

  printf ("constantLogins - client going down after %d\n", count);

  close (socketID);
  free (package);
  return 0;
}

//TODO: clientListEntry alle möglichen Ports

//TODO: clientListEntry alle möglichen Namen bei gleichem Port/ IP

//TODO: Dauerhaft Client List senden

//TODO: 127.0.0.1 und Client Port senden, in der Hoffnung, er verbindet sich auf sich selbst

//TODO: nehme clientList an, ändere alle Namen und schicke sie an alle wieder raus

int main(int argc, char *argv[]) {
  int proto = IPPROTO_TCP;
  in_addr_t listenIP;
	uint16_t listenPort;
	in_addr_t connecttoIP;
	uint16_t connecttoPort;
  int mode = -1;

  printf("I'm an asshole!\n");

  if (argc != 7)
	{
		fprintf (stderr, "Usage:  %s <own IP> <own Port> <own Name> <connect to server ip> <connect to server port> <mode>\n", argv[0]);
		exit(1);
	}

  inet_pton (AF_INET, argv[1], &listenIP); // ip for the clientListEntry
	listenPort = atoi (argv[2]); // open server on port
	//argv[3] -> name
	inet_pton (AF_INET, argv[4], &connecttoIP); // connect to server ip
	connecttoPort = atoi (argv[5]); // connect to server port
  mode = atoi (argv[6]);

  printf ("Mode: %d\n", mode);

  switch (mode) {
    case 10:
      urandom (proto, connecttoIP, connecttoPort); break;
    case 11:
      urandomOften (proto, connecttoIP, connecttoPort); break;
    case 20:
      justConnect (proto, connecttoIP, connecttoPort); break;
    case 21:
      justConnectOften (proto, connecttoIP, connecttoPort); break;
    case 30:
      constantLogins (proto, connecttoIP, connecttoPort); break;
    default:
      printf ("Do nothing.\n"); break;
  }

  sleep (2); // Schlafe 2 Sekunden

  return 0;
}
