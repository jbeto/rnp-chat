#include "antiflood.h"

#include <stdlib.h> // malloc(), free()
#include <stdio.h> //printf()
#include <pthread.h>
#include <string.h> //memcpy()
#include <arpa/inet.h> //inet_ntoa()

AntifloodList* antifloodListFirstEntry = NULL;
AntifloodList* antifloodListLastEntry = NULL;

pthread_mutex_t antifloodListMtx = PTHREAD_MUTEX_INITIALIZER;
int antifloodIsWhitelist = 0;

antifloodIPStatusChangedCallback antifloodStatusChangedFunction;

inline int antifloodListLock(void) {
  return pthread_mutex_lock(&antifloodListMtx);
}

inline int antifloodListUnlock(void) {
  return pthread_mutex_unlock(&antifloodListMtx);
}

inline void antifloodListAddEntry (in_addr_t address) {
  AntifloodList* newEntry = NULL;

  if ((newEntry = malloc(sizeof(AntifloodList))) == NULL) {
    #ifdef DEBUG_MESSAGES
    printf("antifloodListAddEntry - malloc failed\n");
    #endif /* DEBUG_MESSAGES */
    return;
  }
  memcpy(&newEntry->address, &address, sizeof (in_addr_t));
  newEntry->next = NULL;

  if (antifloodListLastEntry != NULL)
  antifloodListLastEntry->next = newEntry;
  antifloodListLastEntry = newEntry;
  if (antifloodListFirstEntry == NULL) {
    antifloodListFirstEntry = newEntry;
  }
}

inline void antifloodListRemoveEntry (in_addr_t address) {
  AntifloodList* entryBefore = NULL;
  AntifloodList* currentEntry = antifloodListFirstEntry;

  while (currentEntry != NULL) {
    if (memcmp (&currentEntry->address, &address, sizeof (in_addr_t)) == 0) {
      if (entryBefore == NULL) {
        antifloodListFirstEntry = currentEntry->next;
      } else {
        entryBefore->next = currentEntry->next;
      }

      if (currentEntry == antifloodListLastEntry) {
        antifloodListLastEntry = entryBefore;
      }

      free(currentEntry);
      return;
    }

    entryBefore = currentEntry;
    currentEntry = currentEntry->next;
  }
}


void antifloodInit (int asWhitelist, antifloodIPStatusChangedCallback statusChangedCallback) {
  antifloodIsWhitelist = asWhitelist;
  antifloodStatusChangedFunction = statusChangedCallback;
}

void antifloodIgnoreIncomingFrom (in_addr_t addressToIgnore) {
  antifloodListLock ();

  if (antifloodIsWhitelist) {
    antifloodListRemoveEntry (addressToIgnore);
  } else {
    antifloodListAddEntry (addressToIgnore);
  }

  antifloodListUnlock();

  (*antifloodStatusChangedFunction) (addressToIgnore, ANTIFLOOD_IPSTATUSTYPE_IGNORE);
}

void antifloodAllowIncomingFrom (in_addr_t addressToAllow) {
  antifloodListLock ();

  if (antifloodIsWhitelist) {
    antifloodListAddEntry (addressToAllow);
  } else {
    antifloodListRemoveEntry (addressToAllow);
  }

  antifloodListUnlock();

  (*antifloodStatusChangedFunction) (addressToAllow, ANTIFLOOD_IPSTATUSTYPE_ALLOW);
}

int antifloodShouldAddressBeIgnored (in_addr_t addressToCheck) {
  AntifloodList* currentEntry;
  int ignore = antifloodIsWhitelist;

  antifloodListLock ();
  currentEntry = antifloodListFirstEntry;

  #ifdef TRACE_MESSAGES
  printf ("antifloodShouldAddressBeIgnored - Search for Address %s\n", inet_ntoa ((struct in_addr) {addressToCheck}));
  #endif /* TRACE_MESSAGES */

  while (currentEntry != NULL) {
    #ifdef TRACE_MESSAGES
    printf ("\tcurrent Entry: %s\n", inet_ntoa ((struct in_addr) {currentEntry->address}));
    #endif /* TRACE_MESSAGES */
    if (memcmp (&currentEntry->address, &addressToCheck, sizeof (in_addr_t)) == 0) {
      #ifdef TRACE_MESSAGES
      printf ("\tfound!\n");
      #endif /* TRACE_MESSAGES */
      ignore = !antifloodIsWhitelist;
      break;
    }

    currentEntry = currentEntry->next;
  }

  antifloodListUnlock();
  return ignore;
}

void antifloodListPrint () {
  AntifloodList* currentEntry;

  antifloodListLock ();
  currentEntry = antifloodListFirstEntry;

  printf ("antifloodList:\n");

  while (currentEntry != NULL) {
    printf ("\t%s\n", inet_ntoa ((struct in_addr) {currentEntry->address}));

    currentEntry = currentEntry->next;
  }

  printf ("antifloodList END\n");

  antifloodListUnlock();
}
