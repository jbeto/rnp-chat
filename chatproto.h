#include <stdint.h> // uint8_t uint16_t
#include <netinet/in.h> // in_addr_t

#ifndef CHATPROTO_H
#define CHATPROTO_H

static const uint8_t CHATPROTO_VERSION = 1;
#define CHATPROTO_NAMEWIDTH 16

typedef struct chatProtoCommonHeaderStruct {
  uint8_t version;
  uint8_t type;
  uint16_t length;
} __attribute__((__packed__)) ChatProtoCommonHeader;

typedef struct chatProtoLoginStruct {
  uint16_t port;
  char name[CHATPROTO_NAMEWIDTH];
} __attribute__((__packed__)) ChatProtoLogin;

typedef struct chatProtoClientListEntryStruct {
  in_addr_t ip;
  uint16_t port;
  char name[CHATPROTO_NAMEWIDTH];
} __attribute__((__packed__)) ChatProtoClientListEntry;

static const uint16_t CHATPROTO_MAX_MESSAGE_CHARS = ((1<<16) - 1 - sizeof(struct chatProtoCommonHeaderStruct))/sizeof(char);
static const uint16_t CHATPROTO_MAX_CLIENTLIST_ENTRYS = ((1<<16) - 1 - sizeof(struct chatProtoCommonHeaderStruct))/sizeof(struct chatProtoClientListEntryStruct);

static const uint8_t CHATPROTO_TYPE_LOGIN = 0;
static const uint8_t CHATPROTO_TYPE_LOGOUT = 1;
static const uint8_t CHATPROTO_TYPE_MESSAGE = 2;
static const uint8_t CHATPROTO_TYPE_CLIENTLIST = 3;

static const uint16_t CHATPROTO_LENGTH_LOGIN = sizeof(ChatProtoCommonHeader) + sizeof(ChatProtoLogin);
static const uint16_t CHATPROTO_LENGTH_LOGOUT = sizeof(ChatProtoCommonHeader);

#endif /* CHATPROTO_H */

int chatProtoGenerateCommonHeaderStruct(ChatProtoCommonHeader* generated, uint8_t type, uint16_t length);
int chatProtoGenerateLoginStruct(ChatProtoLogin* generated, uint16_t port, char name[CHATPROTO_NAMEWIDTH]);
int chatProtoGenerateClientListEntryStruct(ChatProtoClientListEntry* generated, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH]);

void chatProtoPrintCommonHeader(ChatProtoCommonHeader* header);
void chatProtoPrintLogin(ChatProtoLogin* login);
void chatProtoPrintClientListEntry(ChatProtoClientListEntry* clientListEntry);
