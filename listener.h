#include <stdint.h> // uint16_t
#include <arpa/inet.h> // sockaddr_in inet_ntoa()

#include "clientHandler.h"
#include "chatproto.h"

#ifndef LISTENER_H
#define LISTENER_H

#define MAXPENDING 5 // Maximum outstanding connection requests

typedef struct listenerThreadParameterStruct {
  int socketID;
  int proto;
  struct sockaddr_in socketAddress;
  ChatProtoLogin loginForNewClients;
  clientHandlerMessageReceivedCallback msgCallback;
  clientHandlerClientInteractionCallback clientInteractionCallback;
} ListenerThreadParameter;

#endif /* LISTENER_H */

int listenerStart (int proto, in_addr_t ip, uint16_t port, char name[CHATPROTO_NAMEWIDTH], clientHandlerMessageReceivedCallback msgCallback, clientHandlerClientInteractionCallback clientInteractionCallback);
int listenerStop (int socketID);

void* listenerThread(void* param);
